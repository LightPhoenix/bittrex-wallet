//
//  WalletTableViewCell.swift
//  Bittrex Wallet
//
//  Created by Zite Media on 03-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit

class WalletTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCoin: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblPricePiece: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
