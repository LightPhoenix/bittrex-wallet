//
//  AuthenticationViewController.swift
//  Bittrex Wallet
//
//  Created by Jasper Fioole on 08-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit
import TJBioAuthentication

class AuthenticationViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        auth()

        // Do any additional setup after loading the view.
    }

    func auth() {
        TJBioAuthenticator.shared.authenticateUserWithBioMetrics(success: {
            // Biometric Authentication success
            self.performSegue(withIdentifier: "showApp", sender: nil)
        }) { (error) in
            // Biometric Authentication unsuccessful
        }
    }
    
    @IBAction func btnAuthenticate(_ sender: Any) {
        auth() 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
