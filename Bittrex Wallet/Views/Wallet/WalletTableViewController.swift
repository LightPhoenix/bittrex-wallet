//
//  WalletTableViewController.swift
//  Bittrex Wallet
//
//  Created by Zite Media on 03-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit
import BittrexApiKit
import SDWebImage

class WalletTableViewController: UITableViewController {
    
    var data : GetBalancesResponse? = nil //volledige response
    var balances : [Balance]? = nil //gemuteerde response die getoond wordt
    
    var itemsCount : Int = 0 //aantal wallets
    var percentages : Dictionary = [String: Double]() //hoeveel % heeft elke munt gedaan
    var prices : Dictionary = [String: AnyObject]() //hoeveel is elke munt waard
    var bitcoinAmount : Dictionary = [String: Double]() //1 ETH is X btc
    var bitcoinPart : Dictionary = [String: Double]() //Wat is de waarde van 1 ETH in btc
    var totalBitcoin : Double = 0.0 //totaal aantal bitcoins

    var images : Dictionary = [String: String]()
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        getImages()
        
        if(defaults.string(forKey: "setting_Api_Key") == nil || defaults.string(forKey: "setting_Api_Secret") == nil) { //geen api key ingevuld
            self.createAlert(title: "Error", message: "Please fill in your api key/secret")
        } else {
            self.getData()
            self.refreshControl?.beginRefreshing()
        }
        
        
    }
    
    func createAlert(title: String, message: String) {
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func getImages() {
        let urlString = "https://bittrex.com/api/v1.1/public/getmarkets" //url waar afbeeldingen staan
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                
                let result = json["result"]
                
                for row in result  as! [AnyObject] {
                    if(!(row["LogoUrl"] is NSNull)) {
                        self.images[row["MarketCurrency"] as! String] = row["LogoUrl"]! as! String! //stop voor elke coin het plaatje in een array
                    }
                }
                self.images["BTC"] = "https://files.coinmarketcap.com/static/img/coins/128x128/bitcoin.png" //overschijf het plaatje voor bitcoin
                
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
    }
    
    func getPrices() {
        let urlString = "https://blockchain.info/ticker"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            do {
                self.prices = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject] //prijzen gevonden zet ze in een array
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
    }
    
    @objc private func handleRefresh(refreshControl: UIRefreshControl) {
        self.getData()
    }
        
    func getData() {
        
        if((defaults.string(forKey: "setting_Api_Key") == nil) && (defaults.string(forKey: "setting_Api_Secret") == nil)) {
            return
        }
        
        getPrices()
        
        let bittrex = BittrexHelper()
        let api = bittrex.getApi()
        
        self.totalBitcoin  = 0.0
        
        api.getBalances() { (response) in
            switch response{
            case .success(let data):
                
                self.data = data as? GetBalancesResponse
                
                api.getMarketSummaries() { (response) in
                    switch response{
                    case .success(let data):
                        let marketSummaries = data as? GetMarketSummariesResponse
                        
                        var index : Int = 0
                        self.balances = self.data?.balancesData
                        
                        for(currency) in (self.data?.balancesData)! { //Blijf deze array doorlopen, deze word niet gebruikt voor tonen, array mag niet gemuteerd worden tijden loop
                            
                            if(currency.balance <= 0) { //remove empty wallets
                                self.balances?.remove(at: index)
                                continue
                            } //deze array gaan we tonen, we verwijderen lege wallets
                            
                            var marketName : String = "USDT-BTC" //standaard dollar naar btc
                            if((currency.currency) as String != "BTC") { //als het geen bitcoin is dan willen we vanaf bitcoin naar de altcoin rekenen
                               marketName = String(format: "BTC-%@", currency.currency)
                            }
                            
                                for(summary) in (marketSummaries?.marketSummariesData)! {
                                    if(summary.marketName as String == marketName) {
                                        
                                        let percentage = self.percentageGained(new: Double(String(format: "%.8f", summary.last))!, old: Double(String(format: "%.8f", summary.previousDay))!) //procentuele toename/afname
                                        
                                        if((currency.currency) as String != "BTC") {
                                            let btcAmount = summary.last * currency.balance
                                            self.bitcoinPart[String(currency.currency)] = summary.last //Hoeveel btc is dit waard
                                            self.bitcoinAmount[String(currency.currency)] = btcAmount
                                            self.percentages[String(currency.currency)] = percentage
                                            
                                            self.totalBitcoin = self.totalBitcoin + btcAmount
                                        } else {
                                            self.bitcoinAmount[String(currency.currency)] = currency.balance
                                            self.bitcoinPart[String(currency.currency)] = currency.balance
                                            self.totalBitcoin = self.totalBitcoin + currency.balance
                                            self.percentages[String(currency.currency)] = percentage
                                        }
                                    }
                                }
                            index = index + 1
                        }
                        
                        self.itemsCount = (self.balances!.count)
                        
                        self.tableView.reloadData()
                        self.refreshControl?.endRefreshing()
                        
                    case .failed(let err):
                        print(err)
                    }
                }
                                
                self.tableView.reloadData()
                
            case .failed(let err):
                print(err)
            }
    }
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(section == 0) { //section 0 is portfolio waarde
            return 1
        }
        
        return itemsCount
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PortfolioCell", for: indexPath) as! PortfolioTableViewCell
            
            if(self.prices["EUR"] != nil) {
                let price = self.prices["EUR"] as! [String: AnyObject]
                let calcPrice = self.totalBitcoin * Double(truncating: price["last"] as! NSNumber)
                cell.lblPortfolioValue.text = String(format: "€ %.2f", calcPrice)
                
                cell.lblBitcoin.text = String(format: "%g", self.totalBitcoin)

                var totalPercentage = 0.0
                
                    for(index, element) in self.bitcoinAmount {
                        
                        let percentageOfAll = (element / self.totalBitcoin) //Niet * 100 doen dan kunnen we er gelijk mee rekenen
                        let participation = self.percentages[index]! * percentageOfAll
                        
                        totalPercentage = totalPercentage + participation
                        
                    }
                
                cell.lblChange.text = String(format: "%.2f %%", totalPercentage)
                
                if(totalPercentage > 0) {
                    cell.lblChange.textColor = .green
                } else {
                    cell.lblChange.textColor = .red
                }
            }
            
            return cell
        } else {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletTableViewCell
            
            let coinName = (self.balances![indexPath.row].currency) as String
            
            if(self.images[coinName] != nil)
            {
                cell.imgLogo.sd_setImage(with: URL(string: self.images[coinName]!))
            }
            
            cell.lblCoin.text = coinName
            cell.lblAmount.text = forTailingZero(temp: (self.balances![indexPath.row].balance))
            
            if(self.percentages[cell.lblCoin.text!] != nil)
            {
                cell.lblPercentage.text = String(format: "%.2f %%", self.percentages[cell.lblCoin.text!]!)
                
                if(self.percentages[cell.lblCoin.text!]! > 0) {
                    cell.lblPercentage.textColor = .green
                } else {
                    cell.lblPercentage.textColor = .red
                }
            }
            
            if(self.prices["EUR"] != nil && self.bitcoinAmount[cell.lblCoin.text!] != nil) {
                
                let coinName = (self.balances![indexPath.row].currency) as String
                
                let price = self.prices["EUR"] as! [String: AnyObject]
                let calcPrice = Double(self.bitcoinAmount[coinName]!) * Double(truncating: price["last"] as! NSNumber)

                var calcPricePiece = Double(self.bitcoinPart[coinName]!) * Double(truncating: price["last"] as! NSNumber)
                if(coinName == "BTC") {
                    calcPricePiece = Double(truncating: price["last"] as! NSNumber)
                }
                
                cell.lblValue.text = String(format: "€ %.2f", calcPrice)
                
                if(calcPricePiece > 1) {
                    cell.lblPricePiece.text = String(format: "€ %.2f", calcPricePiece)
                } else {
                    cell.lblPricePiece.text = String(format: "€ %.8f", calcPricePiece)
                }
                
            } else if(self.prices["EUR"] != nil && cell.lblCoin.text == "BTC") {
                
                let price = self.prices["EUR"] as! [String: AnyObject]
                let calcPrice = Double((self.balances![indexPath.row].balance) * Double(truncating: price["last"] as! NSNumber))
                cell.lblValue.text = String(format: "€ %.2f", calcPrice)
                
                let calcPricePiece = 1 * Double(truncating: price["last"] as! NSNumber)
                
                if(calcPricePiece > 1) {
                    cell.lblPricePiece.text = String(format: "€ %.2f", calcPricePiece)
                } else {
                    cell.lblPricePiece.text = String(format: "€ %.8f", calcPricePiece)
                }
                
            }
        
            return cell
        }

    }
    
    func forTailingZero(temp: Double) -> String{
        return String(format: "%g", temp)
    }
    
    func percentageGained(new: Double, old: Double) -> Double {
        return ((new - old) / old) * 100
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "settingsSegue")
        {
            let navController: UINavigationController = segue.destination as! UINavigationController
            let destinationController : SettingsViewController = navController.topViewController as! SettingsViewController
            destinationController.setSender(sender: self)
        }
        
    }
 

}

