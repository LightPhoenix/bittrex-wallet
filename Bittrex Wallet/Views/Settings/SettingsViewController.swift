//
//  SettingsViewController.swift
//  Bittrex Wallet
//
//  Created by Jasper Fioole on 04-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var txtApiKey: UITextField!
    @IBOutlet weak var txtApiSecret: UITextField!
    @IBOutlet weak var swhProtectWallet: UISwitch!
    
    let defaults = UserDefaults.standard
    var sender : (Any)? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(red:0.51, green:0.91, blue:0.91, alpha:1.0)
        
        txtApiKey.layer.borderColor = borderColor.cgColor
        txtApiKey.layer.borderWidth = 3
        txtApiKey.layer.cornerRadius = 3
        
        txtApiSecret.layer.borderColor = borderColor.cgColor
        txtApiSecret.layer.borderWidth = 3
        txtApiSecret.layer.cornerRadius = 3
        
        txtApiKey.text = defaults.string(forKey: "setting_Api_Key")
        txtApiSecret.text = defaults.string(forKey: "setting_Api_Secret")
        
        if(defaults.bool(forKey: "setting_Protect_Wallet") == true) {
            swhProtectWallet.setOn(true, animated: false)
        } else {
            swhProtectWallet.setOn(false, animated: false)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setSender(sender: Any) {
       self.sender = sender
    }
    
    @IBAction func swhProtectWallet(_ sender: Any) {
        if(swhProtectWallet.isOn) {
            defaults.set(true, forKey: "setting_Protect_Wallet")
        } else {
            defaults.set(false, forKey: "setting_Protect_Wallet")
        }
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        if((txtApiKey.text?.isEmpty)! || (txtApiSecret.text?.isEmpty)!)
        {
            self.createAlert(title: "Error", message: "Please fill in all required fields")
        }
        else
        {
            defaults.set(txtApiKey.text, forKey: "setting_Api_Key")
            defaults.set(txtApiSecret.text, forKey: "setting_Api_Secret")
            
            let senderController : WalletTableViewController = self.sender as! WalletTableViewController
            senderController.getData()
            senderController.refreshControl?.beginRefreshing()
            
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createAlert(title: String, message: String) {
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
