//
//  PortfolioTableViewCell.swift
//  Bittrex Wallet
//
//  Created by Jasper Fioole on 03-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit

class PortfolioTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPortfolioValue: UILabel!
    @IBOutlet weak var lblBitcoin: UILabel!
    @IBOutlet weak var lblChange: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
