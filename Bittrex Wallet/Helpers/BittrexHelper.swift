//
//  BittrexHelper.swift
//  Bittrex Wallet
//
//  Created by Zite Media on 03-01-18.
//  Copyright © 2018 FutureProDesign. All rights reserved.
//

import UIKit
import BittrexApiKit

class BittrexHelper {
    
    var api : Bittrex? = nil
    let defaults = UserDefaults.standard
    
    func getApi() -> Bittrex {
        let key : String = defaults.string(forKey: "setting_Api_Key")! as String
        let secret : String = defaults.string(forKey: "setting_Api_Secret")! as String
        
//        print(key)
//        print(secret)
        if(self.api == nil) {
            self.api = Bittrex(apikey: key.trimmingCharacters(in: .whitespacesAndNewlines), secretkey: secret.trimmingCharacters(in: .whitespacesAndNewlines))
        }
        return self.api!
    }

}
